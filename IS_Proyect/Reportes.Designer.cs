﻿namespace IS_Proyect
{
    partial class Reportes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DataSet_Reporte = new IS_Proyect.DataSet_Reporte();
            this.Venta_ClienteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Venta_ClienteTableAdapter = new IS_Proyect.DataSet_ReporteTableAdapters.Venta_ClienteTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet_Reporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Venta_ClienteBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DataSet1";
            reportDataSource1.Value = this.Venta_ClienteBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "IS_Proyect.Reporte.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(725, 676);
            this.reportViewer1.TabIndex = 0;
            // 
            // DataSet_Reporte
            // 
            this.DataSet_Reporte.DataSetName = "DataSet_Reporte";
            this.DataSet_Reporte.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // Venta_ClienteBindingSource
            // 
            this.Venta_ClienteBindingSource.DataMember = "Venta_Cliente";
            this.Venta_ClienteBindingSource.DataSource = this.DataSet_Reporte;
            // 
            // Venta_ClienteTableAdapter
            // 
            this.Venta_ClienteTableAdapter.ClearBeforeFill = true;
            // 
            // Reportes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(725, 676);
            this.Controls.Add(this.reportViewer1);
            this.Name = "Reportes";
            this.Text = "Reportes";
            this.Load += new System.EventHandler(this.Reportes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataSet_Reporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Venta_ClienteBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource Venta_ClienteBindingSource;
        private DataSet_Reporte DataSet_Reporte;
        private DataSet_ReporteTableAdapters.Venta_ClienteTableAdapter Venta_ClienteTableAdapter;
    }
}