﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IS_Proyect
{
    public partial class login : Form
    {
        DataTable dt;
        StringBuilder errorMessages = new StringBuilder();

        public login()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if(usertxt.Text == "" && passtxt.Text == "" && comboBox1.Text == "Seleccionar...")
            {
                MessageBox.Show("No ingreso Datos!, porfavor ingrese sus credenciales!");
            }
            else if(usertxt.Text == "" && passtxt.Text == "")
            {
                MessageBox.Show("No ingreso usuario ni contraseña!");
            }
            else if (passtxt.Text == "" && comboBox1.Text == "Seleccionar...")
            {
                MessageBox.Show("No selecciono tipo de usuario ni ingreso contraseña!");
            }
            else if (usertxt.Text == "" && comboBox1.Text == "Seleccionar...")
            {
                MessageBox.Show("No selecciono tipo de usuario ni ingreso usuario!");
            }
            else if(usertxt.Text == "")
            {
                MessageBox.Show("No ingreso Usuario!");
            }
            else if(passtxt.Text == "")
            {
                MessageBox.Show("No ingreso Contraseña!");
            }
            else if(comboBox1.Text == "Seleccionar...")
            {
                MessageBox.Show("No selecciono tipo de usuario!");
            }
            else
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString =
                "Data Source=DESKTOP-0DD8IJK;" +
                "Initial Catalog=multitechgt;" +
                "Integrated Security=SSPI;";
                SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM usuario WHERE usuario = '" + usertxt.Text + "' AND password = '" + passtxt.Text + "' AND tipo = '" + comboBox1.Text + "'", con);
                dt = new DataTable();
                sda.Fill(dt);
                if (dt.Rows.Count == 1)
                {
                    Form1 f1 = new Form1();
                    f1.user = usertxt.Text;
                    f1.tipo = comboBox1.Text;
                    f1.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Las credenciales que ingreso no son correctas, porfavor vuelva a intentarlo!");
                }
            }        

        }
    }
}
