﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IS_Proyect
{
    public partial class Reportes : Form
    {
        public Reportes()
        {
            InitializeComponent();
        }

        public DateTime Fecha { get; set; }

        private void Reportes_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'DataSet_Reporte.Venta_Cliente' Puede moverla o quitarla según sea necesario.
            this.Venta_ClienteTableAdapter.Fill(this.DataSet_Reporte.Venta_Cliente,Fecha);

            this.reportViewer1.RefreshReport();
        }
    }
}
