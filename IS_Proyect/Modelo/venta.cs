//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IS_Proyect.Modelo
{
    using System;
    using System.Collections.Generic;
    
    public partial class venta
    {
        public int codigoventa { get; set; }
        public int codigocliente { get; set; }
        public int codigoarticulo { get; set; }
        public int codigousuario { get; set; }
        public System.DateTime fecha { get; set; }
        public System.TimeSpan hora { get; set; }
        public int cantidad { get; set; }
        public decimal precio_unitario { get; set; }
        public decimal total { get; set; }
        public string observaciones { get; set; }
    
        public virtual cliente cliente { get; set; }
        public virtual inventario inventario { get; set; }
        public virtual usuario usuario { get; set; }
    }
}
