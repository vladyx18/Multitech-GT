﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IS_Proyect
{
    public partial class Form2 : Form
    {

        SqlDataAdapter sda;
        DataTable dt;
        StringBuilder errorMessages = new StringBuilder();

        public Form2()
        {
            InitializeComponent();
            filldatagrid();
            datocombo();
            this.dataGridView1.Columns["precio_compra"].Visible = false;
            this.dataGridView1.Columns["imagen"].Visible = false;
            this.dataGridView1.Columns["precio_venta"].Visible = false;
            this.dataGridView1.Columns["codigoproveedor"].Visible = false;
            this.dataGridView1.Columns["precio_compra"].Visible = false;

        }

        private void filldatagrid()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            sda = new SqlDataAdapter(@"SELECT * FROM inventario", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void bunifuTextbox1_OnTextChange(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            sda = new SqlDataAdapter(@"SELECT * FROM inventario WHERE nombre LIKE '"+bunifuTextbox1.text+"%'", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String ID = comboBox1.SelectedValue.ToString();
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            sda = new SqlDataAdapter(@"SELECT * FROM inventario WHERE codigocategoria = '" + ID + "'", con);
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void datocombo()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            sda = new SqlDataAdapter(@"SELECT codigocategoria, nombre FROM categoria", con);
            dt = new DataTable();
            sda.Fill(dt);
            comboBox1.ValueMember = "codigocategoria";
            comboBox1.DisplayMember = "nombre";
            comboBox1.DataSource = dt;
        }
    }
}
