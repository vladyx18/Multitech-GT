﻿namespace IS_Proyect
{
    partial class UC1
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UC1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            BunifuAnimatorNS.Animation animation5 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation4 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation6 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation3 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation7 = new BunifuAnimatorNS.Animation();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.bunifuTransition1 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuGradientPanel2 = new Bunifu.Framework.UI.BunifuGradientPanel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.bunifuCustomDataGrid1 = new Bunifu.Framework.UI.BunifuCustomDataGrid();
            this.descripcion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fecha = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.bunifuDatepicker1 = new Bunifu.Framework.UI.BunifuDatepicker();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.bunifuTransition2 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuTransition3 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuTransition4 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuTransition5 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.bunifuTransition6 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bunifuTransition7 = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.uCus1 = new IS_Proyect.Uc.UCus();
            this.uccierre1 = new IS_Proyect.Uc.UCCIERRE();
            this.ucc1 = new IS_Proyect.Uc.UCC();
            this.uci1 = new IS_Proyect.Uc.UCI();
            this.uce1 = new IS_Proyect.Uc.UCE();
            this.uca1 = new IS_Proyect.Uc.UCA();
            this.ucp1 = new IS_Proyect.Uc.UCP();
            this.bunifuGradientPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuCustomDataGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // Timer1
            // 
            this.Timer1.Enabled = true;
            this.Timer1.Interval = 1000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // bunifuTransition1
            // 
            this.bunifuTransition1.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.bunifuTransition1.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.bunifuTransition1.DefaultAnimation = animation1;
            // 
            // bunifuGradientPanel2
            // 
            this.bunifuGradientPanel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuGradientPanel2.BackgroundImage")));
            this.bunifuGradientPanel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuGradientPanel2.Controls.Add(this.uCus1);
            this.bunifuGradientPanel2.Controls.Add(this.uccierre1);
            this.bunifuGradientPanel2.Controls.Add(this.ucc1);
            this.bunifuGradientPanel2.Controls.Add(this.uci1);
            this.bunifuGradientPanel2.Controls.Add(this.uce1);
            this.bunifuGradientPanel2.Controls.Add(this.uca1);
            this.bunifuGradientPanel2.Controls.Add(this.ucp1);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox3);
            this.bunifuGradientPanel2.Controls.Add(this.label1);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox4);
            this.bunifuGradientPanel2.Controls.Add(this.label11);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuCustomDataGrid1);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox1);
            this.bunifuGradientPanel2.Controls.Add(this.label9);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox2);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox21);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox20);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox19);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox18);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox17);
            this.bunifuGradientPanel2.Controls.Add(this.label10);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox16);
            this.bunifuGradientPanel2.Controls.Add(this.bunifuDatepicker1);
            this.bunifuGradientPanel2.Controls.Add(this.label8);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox15);
            this.bunifuGradientPanel2.Controls.Add(this.label6);
            this.bunifuGradientPanel2.Controls.Add(this.label5);
            this.bunifuGradientPanel2.Controls.Add(this.label4);
            this.bunifuGradientPanel2.Controls.Add(this.label3);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox13);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox12);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox11);
            this.bunifuGradientPanel2.Controls.Add(this.pictureBox10);
            this.bunifuTransition5.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.bunifuGradientPanel2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuGradientPanel2.GradientBottomLeft = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientBottomRight = System.Drawing.Color.White;
            this.bunifuGradientPanel2.GradientTopLeft = System.Drawing.Color.DimGray;
            this.bunifuGradientPanel2.GradientTopRight = System.Drawing.Color.White;
            this.bunifuGradientPanel2.Location = new System.Drawing.Point(0, 0);
            this.bunifuGradientPanel2.Name = "bunifuGradientPanel2";
            this.bunifuGradientPanel2.Quality = 10;
            this.bunifuGradientPanel2.Size = new System.Drawing.Size(827, 747);
            this.bunifuGradientPanel2.TabIndex = 15;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.LightSlateGray;
            this.bunifuTransition7.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox3.Image = global::IS_Proyect.Properties.Resources.team;
            this.pictureBox3.Location = new System.Drawing.Point(34, 385);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(80, 78);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox3.TabIndex = 44;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LightSlateGray;
            this.bunifuTransition5.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(38, 466);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(73, 19);
            this.label1.TabIndex = 43;
            this.label1.Text = "Usuarios";
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.LightSlateGray;
            this.bunifuTransition7.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox4.Location = new System.Drawing.Point(17, 381);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(116, 110);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 42;
            this.pictureBox4.TabStop = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.bunifuTransition5.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.label11.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(369, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 19);
            this.label11.TabIndex = 35;
            this.label11.Text = "Tabla de Avisos";
            // 
            // bunifuCustomDataGrid1
            // 
            this.bunifuCustomDataGrid1.AllowUserToAddRows = false;
            this.bunifuCustomDataGrid1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuCustomDataGrid1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.bunifuCustomDataGrid1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.bunifuCustomDataGrid1.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.bunifuCustomDataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.bunifuCustomDataGrid1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.SeaGreen;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bunifuCustomDataGrid1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.bunifuCustomDataGrid1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.bunifuCustomDataGrid1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.descripcion,
            this.fecha});
            this.bunifuTransition7.SetDecoration(this.bunifuCustomDataGrid1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.bunifuCustomDataGrid1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.bunifuCustomDataGrid1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.bunifuCustomDataGrid1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.bunifuCustomDataGrid1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.bunifuCustomDataGrid1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.bunifuCustomDataGrid1, BunifuAnimatorNS.DecorationType.None);
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.bunifuCustomDataGrid1.DefaultCellStyle = dataGridViewCellStyle3;
            this.bunifuCustomDataGrid1.DoubleBuffered = true;
            this.bunifuCustomDataGrid1.EnableHeadersVisualStyles = false;
            this.bunifuCustomDataGrid1.GridColor = System.Drawing.Color.Gainsboro;
            this.bunifuCustomDataGrid1.HeaderBgColor = System.Drawing.Color.SeaGreen;
            this.bunifuCustomDataGrid1.HeaderForeColor = System.Drawing.Color.White;
            this.bunifuCustomDataGrid1.Location = new System.Drawing.Point(183, 70);
            this.bunifuCustomDataGrid1.MultiSelect = false;
            this.bunifuCustomDataGrid1.Name = "bunifuCustomDataGrid1";
            this.bunifuCustomDataGrid1.ReadOnly = true;
            this.bunifuCustomDataGrid1.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.bunifuCustomDataGrid1.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.bunifuCustomDataGrid1.RowTemplate.Height = 32;
            this.bunifuCustomDataGrid1.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.bunifuCustomDataGrid1.Size = new System.Drawing.Size(476, 138);
            this.bunifuCustomDataGrid1.TabIndex = 34;
            // 
            // descripcion
            // 
            this.descripcion.DataPropertyName = "descripcion";
            this.descripcion.HeaderText = "Aviso";
            this.descripcion.Name = "descripcion";
            this.descripcion.ReadOnly = true;
            // 
            // fecha
            // 
            this.fecha.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.fecha.DataPropertyName = "fecha";
            this.fecha.HeaderText = "Fecha";
            this.fecha.Name = "fecha";
            this.fecha.ReadOnly = true;
            this.fecha.Width = 216;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Salmon;
            this.bunifuTransition7.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox1.Image = global::IS_Proyect.Properties.Resources.customers2;
            this.pictureBox1.Location = new System.Drawing.Point(578, 256);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 33;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Salmon;
            this.bunifuTransition5.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.label9.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(584, 337);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 19);
            this.label9.TabIndex = 32;
            this.label9.Text = "Clientes";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Salmon;
            this.bunifuTransition7.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox2.Location = new System.Drawing.Point(560, 252);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(116, 110);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 31;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackColor = System.Drawing.Color.MediumPurple;
            this.bunifuTransition7.SetDecoration(this.pictureBox21, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox21, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox21, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox21, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox21, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox21, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox21, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox21.Image = global::IS_Proyect.Properties.Resources.cash_register;
            this.pictureBox21.Location = new System.Drawing.Point(710, 256);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(80, 78);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox21.TabIndex = 28;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Click += new System.EventHandler(this.pictureBox21_Click);
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackColor = System.Drawing.Color.PaleVioletRed;
            this.bunifuTransition7.SetDecoration(this.pictureBox20, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox20, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox20, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox20, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox20, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox20, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox20, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox20.Image = global::IS_Proyect.Properties.Resources.newspaper;
            this.pictureBox20.Location = new System.Drawing.Point(441, 256);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(80, 78);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox20.TabIndex = 27;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Click += new System.EventHandler(this.pictureBox20_Click);
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.bunifuTransition7.SetDecoration(this.pictureBox19, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox19, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox19, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox19, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox19, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox19, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox19, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox19.Image = global::IS_Proyect.Properties.Resources.employee;
            this.pictureBox19.Location = new System.Drawing.Point(301, 256);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(80, 78);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox19.TabIndex = 26;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Click += new System.EventHandler(this.pictureBox19_Click);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackColor = System.Drawing.Color.IndianRed;
            this.bunifuTransition7.SetDecoration(this.pictureBox18, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox18, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox18, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox18, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox18, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox18, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox18, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox18.Image = global::IS_Proyect.Properties.Resources.chat;
            this.pictureBox18.Location = new System.Drawing.Point(168, 256);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(80, 78);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox18.TabIndex = 25;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Click += new System.EventHandler(this.pictureBox18_Click);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackColor = System.Drawing.Color.RoyalBlue;
            this.bunifuTransition7.SetDecoration(this.pictureBox17, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox17, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox17, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox17, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox17, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox17, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox17, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox17.Image = global::IS_Proyect.Properties.Resources.delivery;
            this.pictureBox17.Location = new System.Drawing.Point(34, 256);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(80, 78);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox17.TabIndex = 24;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Click += new System.EventHandler(this.pictureBox17_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.SeaGreen;
            this.bunifuTransition5.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.label10.Font = new System.Drawing.Font("Bahnschrift", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(549, 488);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 45);
            this.label10.TabIndex = 23;
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackColor = System.Drawing.Color.SeaGreen;
            this.bunifuTransition7.SetDecoration(this.pictureBox16, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox16, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox16, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox16, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox16, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox16, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox16, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox16.Location = new System.Drawing.Point(525, 469);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(285, 96);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox16.TabIndex = 22;
            this.pictureBox16.TabStop = false;
            // 
            // bunifuDatepicker1
            // 
            this.bunifuDatepicker1.BackColor = System.Drawing.Color.Indigo;
            this.bunifuDatepicker1.BorderRadius = 0;
            this.bunifuTransition5.SetDecoration(this.bunifuDatepicker1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.bunifuDatepicker1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.bunifuDatepicker1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.bunifuDatepicker1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.bunifuDatepicker1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.bunifuDatepicker1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.bunifuDatepicker1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuDatepicker1.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuDatepicker1.ForeColor = System.Drawing.Color.White;
            this.bunifuDatepicker1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.bunifuDatepicker1.FormatCustom = null;
            this.bunifuDatepicker1.Location = new System.Drawing.Point(525, 427);
            this.bunifuDatepicker1.Name = "bunifuDatepicker1";
            this.bunifuDatepicker1.Size = new System.Drawing.Size(285, 36);
            this.bunifuDatepicker1.TabIndex = 21;
            this.bunifuDatepicker1.Value = new System.DateTime(2018, 5, 13, 18, 54, 20, 0);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.MediumPurple;
            this.bunifuTransition5.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.label8.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(707, 337);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 19);
            this.label8.TabIndex = 17;
            this.label8.Text = "Cierre Caja";
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackColor = System.Drawing.Color.MediumPurple;
            this.bunifuTransition7.SetDecoration(this.pictureBox15, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox15, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox15, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox15, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox15, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox15, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox15, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox15.Location = new System.Drawing.Point(693, 252);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(116, 110);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox15.TabIndex = 15;
            this.pictureBox15.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.PaleVioletRed;
            this.bunifuTransition5.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.label6.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(441, 337);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 19);
            this.label6.TabIndex = 14;
            this.label6.Text = "Inventario";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.bunifuTransition5.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.label5.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(297, 337);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 19);
            this.label5.TabIndex = 13;
            this.label5.Text = "Empleados";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.IndianRed;
            this.bunifuTransition5.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(179, 337);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 19);
            this.label4.TabIndex = 12;
            this.label4.Text = "Avisos";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.RoyalBlue;
            this.bunifuTransition5.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(22, 337);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 19);
            this.label3.TabIndex = 11;
            this.label3.Text = "Proveedores";
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackColor = System.Drawing.Color.PaleVioletRed;
            this.bunifuTransition7.SetDecoration(this.pictureBox13, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox13, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox13, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox13, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox13, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox13, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox13, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox13.Location = new System.Drawing.Point(423, 253);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(116, 110);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox13.TabIndex = 10;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.bunifuTransition7.SetDecoration(this.pictureBox12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox12, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox12.Location = new System.Drawing.Point(284, 252);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(116, 110);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox12.TabIndex = 9;
            this.pictureBox12.TabStop = false;
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackColor = System.Drawing.Color.IndianRed;
            this.bunifuTransition7.SetDecoration(this.pictureBox11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox11, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox11.Location = new System.Drawing.Point(149, 252);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(116, 110);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 8;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackColor = System.Drawing.Color.RoyalBlue;
            this.bunifuTransition7.SetDecoration(this.pictureBox10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.pictureBox10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.pictureBox10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.pictureBox10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.pictureBox10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.pictureBox10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this.pictureBox10, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox10.Location = new System.Drawing.Point(16, 252);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(116, 110);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox10.TabIndex = 7;
            this.pictureBox10.TabStop = false;
            // 
            // bunifuTransition2
            // 
            this.bunifuTransition2.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.bunifuTransition2.Cursor = null;
            animation5.AnimateOnlyDifferences = true;
            animation5.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation5.BlindCoeff")));
            animation5.LeafCoeff = 0F;
            animation5.MaxTime = 1F;
            animation5.MinTime = 0F;
            animation5.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation5.MosaicCoeff")));
            animation5.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation5.MosaicShift")));
            animation5.MosaicSize = 0;
            animation5.Padding = new System.Windows.Forms.Padding(0);
            animation5.RotateCoeff = 0F;
            animation5.RotateLimit = 0F;
            animation5.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation5.ScaleCoeff")));
            animation5.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation5.SlideCoeff")));
            animation5.TimeCoeff = 0F;
            animation5.TransparencyCoeff = 0F;
            this.bunifuTransition2.DefaultAnimation = animation5;
            // 
            // bunifuTransition3
            // 
            this.bunifuTransition3.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.bunifuTransition3.Cursor = null;
            animation4.AnimateOnlyDifferences = true;
            animation4.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.BlindCoeff")));
            animation4.LeafCoeff = 0F;
            animation4.MaxTime = 1F;
            animation4.MinTime = 0F;
            animation4.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.MosaicCoeff")));
            animation4.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation4.MosaicShift")));
            animation4.MosaicSize = 0;
            animation4.Padding = new System.Windows.Forms.Padding(0);
            animation4.RotateCoeff = 0F;
            animation4.RotateLimit = 0F;
            animation4.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.ScaleCoeff")));
            animation4.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation4.SlideCoeff")));
            animation4.TimeCoeff = 0F;
            animation4.TransparencyCoeff = 0F;
            this.bunifuTransition3.DefaultAnimation = animation4;
            // 
            // bunifuTransition4
            // 
            this.bunifuTransition4.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.bunifuTransition4.Cursor = null;
            animation6.AnimateOnlyDifferences = true;
            animation6.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.BlindCoeff")));
            animation6.LeafCoeff = 0F;
            animation6.MaxTime = 1F;
            animation6.MinTime = 0F;
            animation6.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.MosaicCoeff")));
            animation6.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation6.MosaicShift")));
            animation6.MosaicSize = 0;
            animation6.Padding = new System.Windows.Forms.Padding(0);
            animation6.RotateCoeff = 0F;
            animation6.RotateLimit = 0F;
            animation6.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.ScaleCoeff")));
            animation6.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation6.SlideCoeff")));
            animation6.TimeCoeff = 0F;
            animation6.TransparencyCoeff = 0F;
            this.bunifuTransition4.DefaultAnimation = animation6;
            // 
            // bunifuTransition5
            // 
            this.bunifuTransition5.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.bunifuTransition5.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 0F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 0;
            animation2.Padding = new System.Windows.Forms.Padding(0);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 0F;
            this.bunifuTransition5.DefaultAnimation = animation2;
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            // 
            // bunifuTransition6
            // 
            this.bunifuTransition6.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.bunifuTransition6.Cursor = null;
            animation3.AnimateOnlyDifferences = true;
            animation3.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.BlindCoeff")));
            animation3.LeafCoeff = 0F;
            animation3.MaxTime = 1F;
            animation3.MinTime = 0F;
            animation3.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicCoeff")));
            animation3.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicShift")));
            animation3.MosaicSize = 0;
            animation3.Padding = new System.Windows.Forms.Padding(0);
            animation3.RotateCoeff = 0F;
            animation3.RotateLimit = 0F;
            animation3.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.ScaleCoeff")));
            animation3.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.SlideCoeff")));
            animation3.TimeCoeff = 0F;
            animation3.TransparencyCoeff = 0F;
            this.bunifuTransition6.DefaultAnimation = animation3;
            // 
            // bunifuTransition7
            // 
            this.bunifuTransition7.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.bunifuTransition7.Cursor = null;
            animation7.AnimateOnlyDifferences = true;
            animation7.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation7.BlindCoeff")));
            animation7.LeafCoeff = 0F;
            animation7.MaxTime = 1F;
            animation7.MinTime = 0F;
            animation7.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation7.MosaicCoeff")));
            animation7.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation7.MosaicShift")));
            animation7.MosaicSize = 0;
            animation7.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            animation7.RotateCoeff = 0F;
            animation7.RotateLimit = 0F;
            animation7.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation7.ScaleCoeff")));
            animation7.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation7.SlideCoeff")));
            animation7.TimeCoeff = 0F;
            animation7.TransparencyCoeff = 0F;
            this.bunifuTransition7.DefaultAnimation = animation7;
            // 
            // uCus1
            // 
            this.bunifuTransition5.SetDecoration(this.uCus1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.uCus1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.uCus1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.uCus1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.uCus1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.uCus1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.uCus1, BunifuAnimatorNS.DecorationType.None);
            this.uCus1.Location = new System.Drawing.Point(0, 0);
            this.uCus1.Name = "uCus1";
            this.uCus1.Size = new System.Drawing.Size(827, 672);
            this.uCus1.TabIndex = 45;
            this.uCus1.Visible = false;
            // 
            // uccierre1
            // 
            this.bunifuTransition5.SetDecoration(this.uccierre1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.uccierre1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.uccierre1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.uccierre1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.uccierre1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.uccierre1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.uccierre1, BunifuAnimatorNS.DecorationType.None);
            this.uccierre1.Location = new System.Drawing.Point(0, 0);
            this.uccierre1.Name = "uccierre1";
            this.uccierre1.Size = new System.Drawing.Size(827, 672);
            this.uccierre1.TabIndex = 41;
            this.uccierre1.Visible = false;
            // 
            // ucc1
            // 
            this.bunifuTransition5.SetDecoration(this.ucc1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.ucc1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.ucc1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.ucc1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.ucc1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.ucc1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.ucc1, BunifuAnimatorNS.DecorationType.None);
            this.ucc1.Location = new System.Drawing.Point(0, 0);
            this.ucc1.Name = "ucc1";
            this.ucc1.Size = new System.Drawing.Size(827, 672);
            this.ucc1.TabIndex = 38;
            this.ucc1.Visible = false;
            // 
            // uci1
            // 
            this.bunifuTransition5.SetDecoration(this.uci1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.uci1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.uci1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.uci1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.uci1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.uci1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.uci1, BunifuAnimatorNS.DecorationType.None);
            this.uci1.Location = new System.Drawing.Point(0, 0);
            this.uci1.Name = "uci1";
            this.uci1.Size = new System.Drawing.Size(827, 672);
            this.uci1.TabIndex = 40;
            this.uci1.Visible = false;
            // 
            // uce1
            // 
            this.bunifuTransition5.SetDecoration(this.uce1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.uce1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.uce1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.uce1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.uce1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.uce1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.uce1, BunifuAnimatorNS.DecorationType.None);
            this.uce1.Location = new System.Drawing.Point(0, 0);
            this.uce1.Name = "uce1";
            this.uce1.Size = new System.Drawing.Size(827, 672);
            this.uce1.TabIndex = 39;
            this.uce1.Visible = false;
            // 
            // uca1
            // 
            this.bunifuTransition5.SetDecoration(this.uca1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.uca1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.uca1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.uca1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.uca1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.uca1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.uca1, BunifuAnimatorNS.DecorationType.None);
            this.uca1.Location = new System.Drawing.Point(0, 0);
            this.uca1.Name = "uca1";
            this.uca1.Size = new System.Drawing.Size(827, 672);
            this.uca1.TabIndex = 36;
            this.uca1.Visible = false;
            // 
            // ucp1
            // 
            this.bunifuTransition5.SetDecoration(this.ucp1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this.ucp1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this.ucp1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition2.SetDecoration(this.ucp1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this.ucp1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this.ucp1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this.ucp1, BunifuAnimatorNS.DecorationType.None);
            this.ucp1.Location = new System.Drawing.Point(0, 0);
            this.ucp1.Name = "ucp1";
            this.ucp1.Size = new System.Drawing.Size(827, 672);
            this.ucp1.TabIndex = 37;
            this.ucp1.Visible = false;
            // 
            // UC1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bunifuGradientPanel2);
            this.bunifuTransition2.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition1.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition3.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition4.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition5.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition7.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.bunifuTransition6.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.Name = "UC1";
            this.Size = new System.Drawing.Size(827, 672);
            this.Load += new System.EventHandler(this.UC1_Load);
            this.bunifuGradientPanel2.ResumeLayout(false);
            this.bunifuGradientPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuCustomDataGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer Timer1;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition1;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition2;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition3;
        private Bunifu.Framework.UI.BunifuGradientPanel bunifuGradientPanel2;
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuCustomDataGrid bunifuCustomDataGrid1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pictureBox16;
        private Bunifu.Framework.UI.BunifuDatepicker bunifuDatepicker1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox10;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition5;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition4;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.DataGridViewTextBoxColumn descripcion;
        private System.Windows.Forms.DataGridViewTextBoxColumn fecha;
        private Uc.UCP ucp1;
        private Uc.UCC ucc1;
        private Uc.UCE uce1;
        private Uc.UCI uci1;
        private Uc.UCCIERRE uccierre1;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition6;
        private Uc.UCA uca1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox4;
        private Uc.UCus uCus1;
        private BunifuAnimatorNS.BunifuTransition bunifuTransition7;
    }
}
