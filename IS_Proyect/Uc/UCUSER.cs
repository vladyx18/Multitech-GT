﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IS_Proyect.Uc
{
    public partial class UCUSER : UserControl
    {
        SqlDataAdapter sda;
        DataTable dt;

        public UCUSER()
        {
            InitializeComponent();
            Filldatagrid();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label4.Text = DateTime.Now.ToString("hh:mm:ss tt");
            Filldatagrid();
        }

        private void UCUSERcs_Load(object sender, EventArgs e)
        {
            timer1.Start();
            bunifuDatepicker1.Value = DateTime.Now.Date;
            bunifuCustomDataGrid1.DefaultCellStyle.SelectionBackColor = bunifuCustomDataGrid1.DefaultCellStyle.BackColor;
            bunifuCustomDataGrid1.DefaultCellStyle.SelectionForeColor = bunifuCustomDataGrid1.DefaultCellStyle.ForeColor;
        }

        private void Filldatagrid()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            sda = new SqlDataAdapter(@"SELECT descripcion, fecha FROM aviso", con);
            dt = new DataTable();
            sda.Fill(dt);
            bunifuCustomDataGrid1.DataSource = dt;

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //clientes
            bunifuTransition2.ShowSync(ucc1);
        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            //proveedores
            bunifuTransition1.ShowSync(ucp1);
        }

        private void pictureBox21_Click(object sender, EventArgs e)
        {
            //cierre
            bunifuTransition3.ShowSync(uccierre1);
        }

        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            con.Close();
            login l = new login();
            this.Hide();
            l.Show();
        }
    }
}
