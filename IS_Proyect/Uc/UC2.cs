﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Sql;

namespace IS_Proyect
{
    public partial class UC2 : UserControl
    {
        SqlDataAdapter sda;
        DataTable dt;
        StringBuilder errorMessages = new StringBuilder();
        decimal last;

        public UC2()
        {
            InitializeComponent();
            Filldata();
            lblfecha.Text = DateTime.Now.ToShortDateString();
            timer1.Start();
        }

        private void Filldata()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            sda = new SqlDataAdapter(@"SELECT codigocliente, nombre FROM cliente", con);
            dt = new DataTable();
            sda.Fill(dt);
            comboBox1.ValueMember = "codigocliente";
            comboBox1.DisplayMember = "nombre";
            comboBox1.DataSource = dt;
        }

        private void bunifuFlatButton7_Click(object sender, EventArgs e)
        {
           
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String ID = comboBox1.SelectedValue.ToString();
            bunifuMaterialTextbox1.Text = ID;
        }

        private void bunifuFlatButton6_Click(object sender, EventArgs e)
        {
            if (bunifuTextbox1.text == "" || bunifuTextbox1.text == "0")
            {
                MessageBox.Show("No ingreso codigo para buscar producto!");
            }
            else
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString =
                "Data Source=DESKTOP-0DD8IJK;" +
                "Initial Catalog=multitechgt;" +
                "Integrated Security=SSPI;";
                SqlCommand comando = new SqlCommand("SELECT * FROM inventario WHERE codigoarticulo = @codigoarticulo", con);
                comando.Parameters.AddWithValue("@codigoarticulo", bunifuTextbox1.text);
                con.Open();
                SqlDataReader registro = comando.ExecuteReader();
                if (registro.Read())
                {
                    prodtxt.Text = registro["nombre"].ToString();
                    preciotxt.Text = registro["precio_venta"].ToString();
                }
                con.Close();
                bunifuTextbox1.Enabled = false;
            }
        }

        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            if (cantidadtxt.Text == "" || cantidadtxt.Text == "0")
            {
                MessageBox.Show("No se puede agregar el producto, porfavor ingrese la cantidad de producto valida!");
                bunifuTextbox1.Enabled = true;
            }
            else
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString =
                "Data Source=DESKTOP-0DD8IJK;" +
                "Initial Catalog=multitechgt;" +
                "Integrated Security=SSPI;";
                SqlCommand comando = new SqlCommand("SELECT * FROM inventario WHERE codigoarticulo = @codigoarticulo", con);
                comando.Parameters.AddWithValue("@codigoarticulo", bunifuTextbox1.text);
                con.Open();
                SqlDataReader registro = comando.ExecuteReader();
                if (registro.Read())
                {
                    registrotxt.Text = registro["existencias"].ToString();
                }
                con.Close();
                int re1;
                int re2;
                re1 = Convert.ToInt32(registrotxt.Text);
                re2 = Convert.ToInt32(cantidadtxt.Text);
                if (re1 == 0)
                {
                    MessageBox.Show("No hay existencias de este producto!");
                }
                else if (re2 > re1)
                {
                    MessageBox.Show("La cantidad elegida supera la cantidad de existencias que tiene disponible de este producto!");
                }
                else
                {
                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        if (row.Cells[0].Value.ToString() == bunifuTextbox1.text)
                        {
                            row.Cells[3].Value = Convert.ToInt32(row.Cells[3].Value) + Convert.ToInt32(cantidadtxt.Text);
                            double total2;
                            total2 = Convert.ToDouble(row.Cells[2].Value) * Convert.ToInt32(row.Cells[3].Value);
                            row.Cells[4].Value = total2;
                            double a2 = 0;
                            foreach (DataGridViewRow row2 in dataGridView1.Rows)
                            {
                                a2 += Convert.ToDouble(row2.Cells["total"].Value);
                            }
                            bunifuMaterialTextbox2.Text = a2.ToString();
                            goto x;
                        }
                    }
                    double total;
                    total = float.Parse(preciotxt.Text) * int.Parse(cantidadtxt.Text);
                    dataGridView1.Rows.Add(bunifuTextbox1.text, prodtxt.Text, preciotxt.Text, cantidadtxt.Text, total.ToString());
                    double a = 0;
                    foreach (DataGridViewRow row2 in dataGridView1.Rows)
                    {
                        a += Convert.ToDouble(row2.Cells["total"].Value);
                    }
                    bunifuMaterialTextbox2.Text = a.ToString();
                }
                x:
                bunifuTextbox1.text = "";
                prodtxt.Text = "";
                preciotxt.Text = "";
                cantidadtxt.Text = "";
                bunifuTextbox1.Enabled = true;

            }
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count == 0)
            {
                MessageBox.Show("No ha seleccionado productos para poder hacer un borrado");
            }
            else
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.CurrentRow.Index);
                double b = 0;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    b += Convert.ToDouble(row.Cells["total"].Value);
                }
                bunifuMaterialTextbox2.Text = b.ToString();
            }
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.Show();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString =
                "Data Source=DESKTOP-0DD8IJK;" +
                "Initial Catalog=multitechgt;" +
                "Integrated Security=SSPI;";
                con.Open();
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int v1 = Convert.ToInt32(row.Cells[0].Value);
                    double v2 = Convert.ToDouble(row.Cells[2].Value);
                    int v3 = Convert.ToInt32(row.Cells[3].Value);
                    SqlCommand cmd = new SqlCommand("INSERT INTO prodxventa VALUES(@codv,@codp,@cant,@prec,@fecha)", con);
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@codv", last);
                    cmd.Parameters.AddWithValue("@codp", v1);
                    cmd.Parameters.AddWithValue("@cant", v3);
                    cmd.Parameters.AddWithValue("@prec", v2);
                    cmd.Parameters.AddWithValue("@fecha", Convert.ToDateTime(lblfecha.Text));
                    cmd.ExecuteNonQuery();
                    sda = new SqlDataAdapter("SELECT existencias FROM inventario WHERE codigoarticulo = '" + v1 + "'", con);
                    dt = new DataTable();
                    sda.Fill(dt);
                    int ca = int.Parse(dt.Rows[0][0].ToString());
                    int ca2 = ca - v3;
                    SqlCommand cmd3 = new SqlCommand("UPDATE inventario SET existencias = '" + ca2 + "' WHERE codigoarticulo = '" + v1 + "'", con);
                    cmd3.ExecuteNonQuery();
                    ca = 0;
                    ca2 = 0;

                }

                con.Close();
            }
            catch (SqlException ex)
            {
                for (int i = 0; i < ex.Errors.Count; i++)
                {
                    errorMessages.Append("Index #" + i + "\n" +
                        "Message: " + ex.Errors[i].Message + "\n" +
                        "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                        "Source: " + ex.Errors[i].Source + "\n" +
                        "Procedure: " + ex.Errors[i].Procedure + "\n");
                }
                Console.WriteLine(errorMessages.ToString());
            }

            MessageBox.Show("Venta Guardada!");
            bunifuFlatButton7.Text = "Limpiar";
            bunifuFlatButton1.Enabled = false;
        }

        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            if (bunifuMaterialTextbox3.Text == "")
            {
                MessageBox.Show("No se puede realizar la venta por que el valor del efectivo del cliente esta vacio!");
            }
            else if(bunifuMaterialTextbox3.Text == ".")
            {
                MessageBox.Show("No se puede realizar la venta por que el valor del efectivo del cliente es un punto!");

            }
            else if (float.Parse(bunifuMaterialTextbox3.Text) < float.Parse(bunifuMaterialTextbox2.Text))
            {
                MessageBox.Show("No se puede realizar la venta por que el valor del efectivo del cliente es minimo al valor total de la venta!");

            }
            else
            {
                Double res = float.Parse(bunifuMaterialTextbox3.Text) - float.Parse(bunifuMaterialTextbox2.Text);
                bunifuMaterialTextbox4.Text = res.ToString();
                bunifuFlatButton1.Enabled = true;
                bunifuFlatButton4.Enabled = false;
                bunifuMaterialTextbox3.Enabled = false;

                try
                {
                    SqlConnection con = new SqlConnection();
                    con.ConnectionString =
                    "Data Source=DESKTOP-0DD8IJK;" +
                    "Initial Catalog=multitechgt;" +
                    "Integrated Security=SSPI;";
                    SqlCommand cmd = new SqlCommand("Insert into venta values(@codigo,@fecha,@total)", con);
                    con.Open();
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@codigo", bunifuMaterialTextbox1.Text);
                    cmd.Parameters.AddWithValue("@fecha", Convert.ToDateTime(lblfecha.Text));
                    cmd.Parameters.AddWithValue("@total", bunifuMaterialTextbox2.Text);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (SqlException ex)
                {
                    for (int i = 0; i < ex.Errors.Count; i++)
                    {
                        errorMessages.Append("Index #" + i + "\n" +
                            "Message: " + ex.Errors[i].Message + "\n" +
                            "LineNumber: " + ex.Errors[i].LineNumber + "\n" +
                            "Source: " + ex.Errors[i].Source + "\n" +
                            "Procedure: " + ex.Errors[i].Procedure + "\n");
                    }
                    Console.WriteLine(errorMessages.ToString());
                }

                SqlConnection con1 = new SqlConnection();
                con1.ConnectionString =
                "Data Source=DESKTOP-0DD8IJK;" +
                "Initial Catalog=multitechgt;" +
                "Integrated Security=SSPI;";
                SqlCommand cmd1 = new SqlCommand("SELECT IDENT_CURRENT ('venta')", con1);
                con1.Open();
                last = (decimal)cmd1.ExecuteScalar();
                con1.Close();
            }
            

        }

        private void bunifuFlatButton7_Click_1(object sender, EventArgs e)
        {
            bunifuTextbox1.text = "";
            prodtxt.Text = "";
            cantidadtxt.Text = "";
            preciotxt.Text = "";
            bunifuMaterialTextbox2.Text = "0";
            bunifuMaterialTextbox3.Text = "0";
            bunifuMaterialTextbox4.Text = "";
            dataGridView1.Rows.Clear();
            bunifuFlatButton1.Enabled = false;
            bunifuFlatButton4.Enabled = true;
            bunifuMaterialTextbox3.Enabled = true;
            bunifuFlatButton7.Text = "Cancelar venta";
        }

        private void bunifuTextbox1_KeyPress(object sender, EventArgs e)
        {
            if(e.GetType() == typeof(KeyPressEventArgs))
            {
                KeyPressEventArgs ke = (KeyPressEventArgs)e;

                if (Char.IsNumber(ke.KeyChar))
                {
                    ke.Handled = false;
                }
                else if (Char.IsControl(ke.KeyChar))
                {
                    ke.Handled = false;
                }
                else
                {
                    ke.Handled = true;
                }
            }

        }

        private void cantidadtxt_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.GetType() == typeof(KeyPressEventArgs))
            {
                KeyPressEventArgs ke = (KeyPressEventArgs)e;

                if (Char.IsNumber(ke.KeyChar))
                {
                    ke.Handled = false;
                }
                else if (Char.IsControl(ke.KeyChar))
                {
                    ke.Handled = false;
                }
                else
                {
                    ke.Handled = true;
                }
            }
        }

        private void bunifuMaterialTextbox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.GetType() == typeof(KeyPressEventArgs))
            {
                KeyPressEventArgs ke = (KeyPressEventArgs)e;

                if (Char.IsNumber(ke.KeyChar))
                {
                    ke.Handled = false;
                }
                else if (Char.IsControl(ke.KeyChar))
                {
                    ke.Handled = false;
                }
                else if(ke.KeyChar == '.')
                {
                    ke.Handled = false;
                }
                else
                {
                    ke.Handled = true;
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if(dataGridView1.Rows.Count == 0)
            {
                bunifuFlatButton4.Enabled = false;
            }
            else
            {
                bunifuFlatButton4.Enabled = true;
            }
        }
    }
}
