﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IS_Proyect
{
    public partial class UC1 : UserControl
    {
        SqlDataAdapter sda;
        DataTable dt;

        public UC1()
        {
            InitializeComponent();
            Filldatagrid();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            label10.Text = DateTime.Now.ToString("hh:mm:ss tt");
            Filldatagrid();
        }

        private void UC1_Load(object sender, EventArgs e)
        {
            Timer1.Start();
            bunifuDatepicker1.Value = DateTime.Now.Date;
            bunifuCustomDataGrid1.DefaultCellStyle.SelectionBackColor = bunifuCustomDataGrid1.DefaultCellStyle.BackColor;
            bunifuCustomDataGrid1.DefaultCellStyle.SelectionForeColor = bunifuCustomDataGrid1.DefaultCellStyle.ForeColor;
        }

        private void pictureBox18_Click(object sender, EventArgs e)
        {
            //avisos
            bunifuTransition1.ShowSync(uca1);
        }

        private void Filldatagrid()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            sda = new SqlDataAdapter(@"SELECT descripcion, fecha FROM aviso", con);
            dt = new DataTable();
            sda.Fill(dt);
            bunifuCustomDataGrid1.DataSource = dt;

        }

        private void pictureBox17_Click(object sender, EventArgs e)
        {
            //proveedores
            bunifuTransition2.ShowSync(ucp1);
        }

        private void pictureBox19_Click(object sender, EventArgs e)
        {
            //empleados
            bunifuTransition4.ShowSync(uce1);
        }

        private void pictureBox20_Click(object sender, EventArgs e)
        {
            //inventario
            bunifuTransition5.ShowSync(uci1);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //clientes
            bunifuTransition3.ShowSync(ucc1);
        }

        private void pictureBox21_Click(object sender, EventArgs e)
        {
            //cierre
            bunifuTransition6.ShowSync(uccierre1);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            //usuarios
            bunifuTransition7.ShowSync(uCus1);
        }
    }
}
