﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;

namespace IS_Proyect.Uc
{
    public partial class UCCIERRE : UserControl
    {
        SqlDataAdapter sda;
        DataTable dt;

        public UCCIERRE()
        {
            InitializeComponent();
            bunifuDatepicker1.Value = DateTime.Now.Date;
            dataGridView1.Columns["codigoventa"].Visible = false;
        }

        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Filldatagrid()
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            sda = new SqlDataAdapter(@"SELECT * FROM venta WHERE fecha = @fecha", con);
            sda.SelectCommand.Parameters.AddWithValue("@fecha", Convert.ToDateTime(bunifuDatepicker1.Value.ToShortDateString()));
            con.Open();
            dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            bunifuTransition1.HideSync(this);

        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            Filldatagrid();
            double a = 0;
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                a += Convert.ToDouble(row.Cells["total"].Value);
            }
            bunifuMaterialTextbox2.Text = a.ToString();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            Reportes r = new Reportes();
            r.Fecha = bunifuDatepicker1.Value;
            r.ShowDialog();
        }

    }
}

