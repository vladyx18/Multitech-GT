﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace IS_Proyect
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

        }

        public String user { get; set; }
        public String tipo { get; set; }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            DialogResult result;
            result = MessageBox.Show("Esta seguro que quiere salir?", "Aviso", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString =
                "Data Source=DESKTOP-0DD8IJK;" +
                "Initial Catalog=multitechgt;" +
                "Integrated Security=SSPI;";
                con.Close();
                Application.Exit();
            }
            else if (result == System.Windows.Forms.DialogResult.No)
            {
                MessageBox.Show("Operacion abortada!");
            }
        }

        private void PictureBox2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            if(tipo == "administrador")
            {
                uC21.Visible = false;
                uC31.Visible = false;
                uC41.Visible = false;
                ucuser1.Visible = false;
                pictureBox4.Visible = true;
                pictureBox5.Visible = false;
                pictureBox6.Visible = false;
                pictureBox7.Visible = false;
                bunifuFlatButton1.IsTab = true;
                bunifuTransition1.ShowSync(uC11);
            }
            else if(tipo == "usuario")
            {
                uC11.Visible = false;
                uC21.Visible = false;
                uC31.Visible = false;
                uC41.Visible = false;
                pictureBox4.Visible = true;
                pictureBox5.Visible = false;
                pictureBox6.Visible = false;
                pictureBox7.Visible = false;
                bunifuFlatButton1.IsTab = true;
                bunifuTransition1.ShowSync(ucuser1);
            }
            

        }

        private void bunifuFlatButton2_Click(object sender, EventArgs e)
        {
            bunifuFlatButton1.Normalcolor = Color.Transparent;
            uC11.Visible = false;
            uC31.Visible = false;
            uC41.Visible = false;
            ucuser1.Visible = false;
            pictureBox6.Visible = false;
            pictureBox7.Visible = false;
            pictureBox4.Visible = false;
            bunifuFlatButton2.IsTab = true;
            pictureBox5.Visible = true;
            bunifuTransition2.ShowSync(uC21);
        }

        private void bunifuFlatButton3_Click(object sender, EventArgs e)
        {
            bunifuFlatButton1.Normalcolor = Color.Transparent;
            uC11.Visible = false;
            uC21.Visible = false;
            uC41.Visible = false;
            ucuser1.Visible = false;
            pictureBox5.Visible = false;
            pictureBox7.Visible = false;
            pictureBox4.Visible = false;
            bunifuFlatButton3.IsTab = true;
            pictureBox6.Visible = true;
            bunifuTransition3.ShowSync(uC31);
        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {
            bunifuFlatButton1.Normalcolor = Color.Transparent;
            uC11.Visible = false;
            uC21.Visible = false;
            uC31.Visible = false;
            ucuser1.Visible = false;
            pictureBox5.Visible = false;
            pictureBox6.Visible = false;
            pictureBox4.Visible = false;
            bunifuFlatButton4.IsTab = true;
            pictureBox7.Visible = true;
            bunifuTransition4.ShowSync(uC41);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Text ="Usuario: "+user;
            label2.Text ="Tipo: "+tipo;
            pictureBox4.Visible = true;
            bunifuFlatButton1.Normalcolor = Color.FromArgb(31, 43, 55);
            if (tipo == "administrador")
            {
                uC11.Visible = true;
            }
            else if (tipo == "usuario")
            {
                ucuser1.Visible = true;          
            }
            
        }

        private void bunifuCustomLabel1_Click(object sender, EventArgs e)
        {
            
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void bunifuGradientPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void bunifuFlatButton5_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection();
            con.ConnectionString =
            "Data Source=DESKTOP-0DD8IJK;" +
            "Initial Catalog=multitechgt;" +
            "Integrated Security=SSPI;";
            con.Close();
            login l = new login();
            this.Hide();
            l.Show();
        }
    }
}
